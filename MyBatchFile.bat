@echo off
SETLOCAL ENABLEDELAYEDEXPANSION

@set projectVersion=%1

if not defined DevEnvDir (
	IF EXIST "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\Tools\vsdevcmd.bat" (
		call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\Tools\vsdevcmd.bat"
		goto :build
	) 
	IF EXIST "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\vsdevcmd.bat" (
		call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\vsdevcmd.bat"
		goto :build
	) 
	IF EXIST "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\Common7\Tools\vsdevcmd.bat" (
		call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\Common7\Tools\vsdevcmd.bat"
		goto :build
	) 
	IF EXIST "C:\Program Files (x86)\Microsoft Visual Studio\2019\Enterprise\Common7\Tools\vsdevcmd.bat" (
		call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Enterprise\Common7\Tools\vsdevcmd.bat"
		goto :build
	)
)
:build

if not defined DevEnvDir (
	exit /b 1;
)

@IF NOT %ERRORLEVEL% == 0 EXIT /b %ERRORLEVEL%
msbuild ManojDemoProject.sln /t:restore /t:Build /p:Configuration=Release;AssemblyFileVersion=%projectVersion% /m /nr:false
EXIT /b %ERRORLEVEL%

ENDLOCAL